<table class="table">
    <thead>
    <th>Title</th>
			<th>Body</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($posts as $post)
        <tr>
            <td>{!! $post->title !!}</td>
			<td>{!! $post->body !!}</td>
            <td>
                <a href="{!! route('posts.edit', [$post->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('posts.delete', [$post->id]) !!}" onclick="return confirm('Are you sure wants to delete this Post?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
